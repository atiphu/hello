const express = require("express");
const cors = require("cors");

const app = express();
app.use(cors());
app.use(express.urlencoded({ extended: true })); //Limit Buffer
app.use(express.json()); //Limit Buffer

app.get("/api/hello", (req, res) => {
  res.send({
    statusCode: 200,
    messege: "Hello World",
  });
});

var port = process.env.PORT || 1245;
app.listen(port, "0.0.0.0", () =>
  console.log(`listening at http://localhost:${port}`)
);

// ใช้ตรวจสอบ path
app.use((req, res, next) => {
  var err = new Error("ไม่พบ path ที่คุณต้องการ");
  err.status = 404;
  next(err);
});
